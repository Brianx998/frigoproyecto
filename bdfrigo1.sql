-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.11-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para bd_frigo
CREATE DATABASE IF NOT EXISTS `bd_frigo` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci */;
USE `bd_frigo`;

-- Volcando estructura para tabla bd_frigo.area
CREATE TABLE IF NOT EXISTS `area` (
  `id_area` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_frigo.area: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
/*!40000 ALTER TABLE `area` ENABLE KEYS */;

-- Volcando estructura para tabla bd_frigo.clasificacion
CREATE TABLE IF NOT EXISTS `clasificacion` (
  `id_clasificacion` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`id_clasificacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_frigo.clasificacion: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `clasificacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `clasificacion` ENABLE KEYS */;

-- Volcando estructura para tabla bd_frigo.consulta_usuario
CREATE TABLE IF NOT EXISTS `consulta_usuario` (
  `id_consulta` int(11) NOT NULL AUTO_INCREMENT,
  `id_cedula` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id_consulta`),
  KEY `FK-id_cedula` (`id_cedula`),
  KEY `FK-id_tipo` (`id_tipo`),
  CONSTRAINT `FK-id_cedula` FOREIGN KEY (`id_cedula`) REFERENCES `usuarios` (`id_cedula`),
  CONSTRAINT `FK-id_tipo` FOREIGN KEY (`id_tipo`) REFERENCES `tipo` (`id_tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_frigo.consulta_usuario: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `consulta_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `consulta_usuario` ENABLE KEYS */;

-- Volcando estructura para tabla bd_frigo.tipo
CREATE TABLE IF NOT EXISTS `tipo` (
  `id_tipo` int(11) NOT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `Id_area` int(11) DEFAULT NULL,
  `id_clasificacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_tipo`),
  KEY `Id_area` (`Id_area`),
  KEY `id_clasificacion` (`id_clasificacion`),
  CONSTRAINT `FK-id_area` FOREIGN KEY (`Id_area`) REFERENCES `area` (`id_area`),
  CONSTRAINT `FK-id_clasificacion` FOREIGN KEY (`id_clasificacion`) REFERENCES `clasificacion` (`id_clasificacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_frigo.tipo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo` ENABLE KEYS */;

-- Volcando estructura para tabla bd_frigo.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_cedula` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `contraseña` varchar(50) NOT NULL,
  `admin` bit(1) NOT NULL,
  `privilegio` bit(1) NOT NULL,
  `profesion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_cedula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_frigo.usuarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
consulta_usuario