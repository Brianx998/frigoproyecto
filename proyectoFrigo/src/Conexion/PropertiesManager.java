/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

import java.awt.HeadlessException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import javax.swing.JOptionPane;

/**
 *
 * @author sebas
 */
class PropertiesManager {
    
    public PropertiesManager() {
    }
   
    
    public HashMap<String, String> getProperties(){
        
        HashMap<String, String> properties = new HashMap<>();
        try {
            Properties props = new Properties();
            props.load(getClass().getResourceAsStream("conexion.properties"));
            
            //si el archivo no esta vacio
            if(! props.isEmpty()){
                properties.put("usuario", props.getProperty("usuario"));
                properties.put("password", props.getProperty("password"));
                properties.put("puerto", props.getProperty("puerto"));
                properties.put("servidor", props.getProperty("servidor"));
                properties.put("bd", props.getProperty("bd"));
            }
            else{
                JOptionPane.showMessageDialog(null,"el archivo de propiedades no esta configurado correctamente","error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (HeadlessException | IOException e) {
            JOptionPane.showMessageDialog(null,e.getMessage(),"error", JOptionPane.ERROR_MESSAGE);
        }
        return properties;
        
    }
    public static void main(String[] args) {
        PropertiesManager propertiesManager = new PropertiesManager();
        HashMap<String, String> properties = propertiesManager.getProperties();
        System.out.println("usuario: "+properties.get("usuario"));
        System.out.println("password: "+properties.get("password"));
        System.out.println("servidor: "+properties.get("servidor"));
        System.out.println("puerto: "+properties.get("puerto"));
        System.out.println("bd: "+properties.get("bd"));
    }
    
}
