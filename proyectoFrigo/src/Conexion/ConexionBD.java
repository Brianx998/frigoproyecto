/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import javax.swing.JOptionPane;

/**
 *
 * @author sebas
 */
public class ConexionBD {

    public static Connection connection;
    public static Statement statement;
    HashMap<String, String> properties;

    public ConexionBD() {
        PropertiesManager propertiesManager = new PropertiesManager();
        properties = propertiesManager.getProperties();

    }

    /**
     * permite conectar una base de datos
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    public void conectar() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        connection = DriverManager.getConnection("jdbc:mysql://" + properties.get("servidor") + ":"
                + properties.get("puerto") + "/" + properties.get("bd"),
                properties.get("usuario"), properties.get("password"));

        statement = connection.createStatement();
        System.out.println("conectado...");
    }

    public void desconectar() {
        try {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
            System.out.println("desconetado...");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "no pudo desconectarse de la base de datos");
        }
    }

}
