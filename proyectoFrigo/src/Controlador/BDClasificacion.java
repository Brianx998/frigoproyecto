/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.ConexionBD;
import Modelo.Clasificacion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import static Conexion.ConexionBD.connection;
import static Conexion.ConexionBD.statement;

/**
 *
 * @author braia
 */
public class BDClasificacion extends ConexionBD{

    public BDClasificacion() {
        super();
    }
    
 private final String TABLE_NAME = "clasificacion";

    /**
     * 
     * @return 
     */
    public List<Clasificacion> consultarTodos() {
        List<Clasificacion> resultado = new ArrayList<>();

        try {
            conectar();
            String query = "select * from " + TABLE_NAME;
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Clasificacion clasificacion = new Clasificacion();
                clasificacion.setId_clasificacion(resultSet.getInt("id_clasificacion"));
                clasificacion.setDescripcion(resultSet.getString("descripcion"));

                resultado.add(clasificacion);

            }
            resultSet.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "error en la consulta sql " + e.getMessage());
        } finally {
            desconectar();
        }
        return resultado;
    }

    /**
     * 
     * @param clasificacion
     */
    public void insertar(Clasificacion clasificacion) {
        try {
            conectar();
            String query = "insert into " + TABLE_NAME + " values (?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            int posicion = 0;

            preparedStatement.setInt(++posicion, clasificacion.getId_clasificacion());
            preparedStatement.setString(++posicion, clasificacion.getDescripcion());

            preparedStatement.executeUpdate();

            preparedStatement.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "error  " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "error en la consulta sql " + e.getMessage());
        } finally {
            desconectar();
        }
    }

    /**
     * 
     * @param clasificacion 
     */
    public void actualizarClasificacion(Clasificacion clasificacion) {

        try {
            conectar();

            String query = "update " + TABLE_NAME + " set  descripcion=? where id_clasificacion=?";
            PreparedStatement preparedstatement = connection.prepareStatement(query);
            int pos = 0;

            preparedstatement.setString(++pos, clasificacion.getDescripcion());
            preparedstatement.setInt(++pos, clasificacion.getId_clasificacion());

            preparedstatement.executeUpdate();
            preparedstatement.close();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }

    }

    /**
     * 
     * @param id_clasificacion
     */
    public void eliminarClasificacion(int id_clasificacion) {

        try {
            conectar();

            String query = "delete from " + TABLE_NAME + " where id_clasificacion=?";
            PreparedStatement preparedstatement = connection.prepareStatement(query);
            preparedstatement.setLong(1, id_clasificacion);
            preparedstatement.executeUpdate();
            preparedstatement.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }

    }
/**
 * 
 * @param id_clasificacion
 * @return 
 */
    public Clasificacion consultarClasificacion(int id_clasificacion) {
        Clasificacion clasificacion = new Clasificacion();

        try {
            conectar();

            String query = "select * from " + TABLE_NAME + " where id_clasificacion=?";
            PreparedStatement preparedstatement = connection.prepareStatement(query);
            preparedstatement.setLong(1, id_clasificacion);
            ResultSet resultset = preparedstatement.executeQuery();

            if (resultset.next()) {
                clasificacion.setId_clasificacion(resultset.getInt("id_clasificacion"));
                clasificacion.setDescripcion(resultset.getString("descripcion"));

            }

            resultset.close();
            preparedstatement.close();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }
        return clasificacion;
    }
}