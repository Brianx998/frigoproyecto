/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.ConexionBD;
import Modelo.Area;
import Modelo.ConsultaUsuario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import static Conexion.ConexionBD.connection;
import static Conexion.ConexionBD.statement;

/**
 *
 * @author braia
 */
public class BDConsultaUsuario extends ConexionBD{
   
    public BDConsultaUsuario() {
        super();
    }

    private final String TABLE_NAME = "consultausuario";

    /**
     * 
     * @return 
     */
    public List<ConsultaUsuario> consultarTodos() {
        List<ConsultaUsuario> resultado = new ArrayList<>();

        try {
            conectar();
            String query = "select * from " + TABLE_NAME;
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                ConsultaUsuario consultausuario = new ConsultaUsuario();
                consultausuario.setId_consulta(resultSet.getInt("id_consutausuarios"));
                consultausuario.setId_cedula(resultSet.getInt("id_cedula"));
                consultausuario.setId_tipo(resultSet.getInt("id_tipo"));
                consultausuario.setFecha(resultSet.getString("fecha"));

                resultado.add(consultausuario);

            }
            resultSet.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "error en la consulta sql " + e.getMessage());
        } finally {
            desconectar();
        }
        return resultado;
    }

    /**
     * 
     * @param consultaUsuario 
     */
    public void insertar(ConsultaUsuario consultaUsuario) {
        try {
            conectar();
            String query = "insert into " + TABLE_NAME + " values (?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            int posicion = 0;

            preparedStatement.setInt(++posicion, consultaUsuario.getId_consulta());
            preparedStatement.setInt(++posicion, consultaUsuario.getId_cedula());
            preparedStatement.setInt(++posicion, consultaUsuario.getId_tipo()); 
            preparedStatement.setString(++posicion, consultaUsuario.getFecha());

            preparedStatement.executeUpdate();

            preparedStatement.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "error  " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "error en la consulta sql " + e.getMessage());
        } finally {
            desconectar();
        }
    }

    /**
     * 
     * @param consultaUsuario 
     */
    public void actualizarConsultaUsuario(ConsultaUsuario consultaUsuario) {

        try {
            conectar();

            String query = "update " + TABLE_NAME + " set  descripcion=? where id_area=?";
            PreparedStatement preparedstatement = connection.prepareStatement(query);
            int pos = 0;

            preparedstatement.setInt(++pos, consultaUsuario.getId_consulta()); 
            preparedstatement.setInt(++pos, consultaUsuario.getId_cedula());
            preparedstatement.setInt(++pos, consultaUsuario.getId_tipo());
            preparedstatement.setString(++pos, consultaUsuario.getFecha());

            preparedstatement.executeUpdate();
            preparedstatement.close();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }

    }

    /**
     * 
     * @param id_consulta
     */
    public void eliminarConsultaUsuario(int id_consulta) {

        try {
            conectar();

            String query = "delete from " + TABLE_NAME + " where id_area=?";
            PreparedStatement preparedstatement = connection.prepareStatement(query);
            preparedstatement.setLong(1, id_consulta);
            preparedstatement.executeUpdate();
            preparedstatement.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }

    }
/**
 * 
     * @param id_consulta
 * @return 
 */
    public ConsultaUsuario consultarConsultaUsuario(int id_consulta) {
        ConsultaUsuario consultaUsuario = new ConsultaUsuario();

        try {
            conectar();

            String query = "select * from " + TABLE_NAME + " where id_consulta=?";
            PreparedStatement preparedstatement = connection.prepareStatement(query);
            preparedstatement.setLong(1, id_consulta);
            ResultSet resultset = preparedstatement.executeQuery();

            if (resultset.next()) {
                consultaUsuario.setId_consulta(resultset.getInt("id_consulta"));
                consultaUsuario.setId_cedula(resultset.getInt("id_cedula"));
                consultaUsuario.setId_tipo(resultset.getInt("id_tipo")); 
                consultaUsuario.setFecha(resultset.getString("fecha"));

            }

            resultset.close();
            preparedstatement.close();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }
        return consultaUsuario;
    }
}
