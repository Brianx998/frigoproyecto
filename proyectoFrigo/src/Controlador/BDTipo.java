/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.ConexionBD;
import Modelo.Tipo;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import static Conexion.ConexionBD.connection;
import static Conexion.ConexionBD.statement;

/**
 *
 * @author braia
 */
public class BDTipo extends ConexionBD{
    
 public BDTipo() {
        super();
    }
    
 private final String TABLE_NAME = "tipo";

    /**
     * 
     * @return 
     */
    public List<Tipo> consultarTodos() {
        List<Tipo> resultado = new ArrayList<>();

        try {
            conectar();
            String query = "select * from " + TABLE_NAME;
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Tipo tipo = new Tipo();
                tipo.setId_tipo(resultSet.getInt("id_tipo"));
                tipo.setDescripcion(resultSet.getString("descripcion"));
                tipo.setId_area(resultSet.getInt("id_area"));
                tipo.setId_clasificacion(resultSet.getInt("id_clasificacion"));

                resultado.add(tipo);

            }
            resultSet.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "error en la consulta sql " + e.getMessage());
        } finally {
            desconectar();
        }
        return resultado;
    }

    /**
     * 
     * @param tipo
     */
    public void insertar(Tipo tipo) {
        try {
            conectar();
            String query = "insert into " + TABLE_NAME + " values (?, ?, ? )";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            int posicion = 0;

            preparedStatement.setInt(++posicion, tipo.getId_tipo());
            preparedStatement.setString(++posicion, tipo.getDescripcion());
            preparedStatement.setInt(++posicion, tipo.getId_area());
            preparedStatement.setInt(++posicion, tipo.getId_clasificacion());

            preparedStatement.executeUpdate();

            preparedStatement.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "error  " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "error en la consulta sql " + e.getMessage());
        } finally {
            desconectar();
        }
    }

    /**
     * 
     * @param tipo
     */
    public void actualizarTipo(Tipo tipo) {

        try {
            conectar();

            String query = "update " + TABLE_NAME + " set  descripcion=? where id_tipo=?";
            PreparedStatement preparedstatement = connection.prepareStatement(query);
            int pos = 0;

            preparedstatement.setString(++pos, tipo.getDescripcion());
            preparedstatement.setInt(++pos, tipo.getId_tipo());
            preparedstatement.setInt(++pos, tipo.getId_area());
            preparedstatement.setInt(++pos, tipo.getId_clasificacion());

            
            preparedstatement.executeUpdate();
            preparedstatement.close();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }

    }

    /**
     * 
     * @param id_tipo
     */
    public void eliminarTipo(int id_tipo) {

        try {
            conectar();

            String query = "delete from " + TABLE_NAME + " where id_tipo=?";
            PreparedStatement preparedstatement = connection.prepareStatement(query);
            preparedstatement.setLong(1, id_tipo);
            preparedstatement.executeUpdate();
            preparedstatement.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }

    }
/**
 * 
 * @param id_tipo 
 * @return 
 */
    public Tipo consultarTipo(int id_tipo) {
        Tipo tipo = new Tipo();

        try {
            conectar();

            String query = "select * from " + TABLE_NAME + " where id_tipo=?";
            PreparedStatement preparedstatement = connection.prepareStatement(query);
            preparedstatement.setLong(1, id_tipo);
            ResultSet resultset = preparedstatement.executeQuery();

            if (resultset.next()) {
                tipo.setId_tipo(resultset.getInt("id_tipo"));
                tipo.setId_area(resultset.getInt("id_area"));
                tipo.setId_clasificacion(resultset.getInt("id_clasificacion"));
                tipo.setDescripcion(resultset.getString("descripcion"));

            }

            resultset.close();
            preparedstatement.close();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }
        return tipo;
    }

}
