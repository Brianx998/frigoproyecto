/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.ConexionBD;
import Modelo.Usuarios;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import static Conexion.ConexionBD.connection;
import static Conexion.ConexionBD.statement;

/**
 *
 * @author braia
 */
public class BDUsuarios extends ConexionBD {

    public BDUsuarios() {
        super();
    }

    private final String TABLE_NAME = "usuarios";

    /**
     *
     * @return
     */
    public List<Usuarios> consultarTodos() {
        List<Usuarios> resultado = new ArrayList<>();

        try {
            conectar();
            String query = "select * from " + TABLE_NAME;
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Usuarios usuarios = new Usuarios();
                usuarios.setId_cedula(resultSet.getInt("id_cedula"));
                usuarios.setNombre(resultSet.getString("nombre"));
                usuarios.setApellido(resultSet.getString("apellido"));
                usuarios.setContraseña(resultSet.getString("contraseña"));
                usuarios.setProfesion(resultSet.getString("profesion"));
                usuarios.setAdmin(resultSet.getString("admin"));
                usuarios.setPrivilegio(resultSet.getString("privilegio"));

                resultado.add(usuarios);

            }
            resultSet.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "error en la consulta sql " + e.getMessage());
        } finally {
            desconectar();
        }
        return resultado;
    }

    /**
     *
     * @param usuarios
     */
    public void insertar(Usuarios usuarios) {
        try {
            conectar();
            String query = "insert into " + TABLE_NAME + " values (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            int posicion = 0;

            preparedStatement.setInt(++posicion, usuarios.getId_cedula());
            preparedStatement.setString(++posicion, usuarios.getNombre());
            preparedStatement.setString(++posicion, usuarios.getApellido());
            preparedStatement.setString(++posicion, usuarios.getContraseña());
            preparedStatement.setString(++posicion, usuarios.getAdmin());
            preparedStatement.setString(++posicion, usuarios.getPrivilegio());
            preparedStatement.setString(++posicion, usuarios.getProfesion());

            preparedStatement.executeUpdate();

            preparedStatement.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "error  " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "error en la consulta sql " + e.getMessage());
        } finally {
            desconectar();
        }
    }

    /**
     *
     * @param usuarios
     */
    public void actualizarUsuarios(Usuarios usuarios) {

        try {
            conectar();

            String query = "update " + TABLE_NAME + " set  nombre=?, apellido=?,"
                    + " contraseña=?, profesion=?, admin=?, privilegio=?  where id_cedula=?";
            PreparedStatement preparedstatement = connection.prepareStatement(query);
            int pos = 0;

            
            preparedstatement.setString(++pos, usuarios.getNombre());
            preparedstatement.setString(++pos, usuarios.getApellido());
            preparedstatement.setString(++pos, usuarios.getContraseña());
            preparedstatement.setString(++pos, usuarios.getProfesion());
            preparedstatement.setString(++pos, usuarios.getAdmin());
            preparedstatement.setString(++pos, usuarios.getPrivilegio());
            preparedstatement.setInt(++pos, usuarios.getId_cedula());
            

            preparedstatement.executeUpdate();
            preparedstatement.close();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }

    }

    /**
     *
     * @param id_cedula
     */
    public void eliminarUsuarios(int id_cedula) {

        try {
            conectar();

            String query = "delete from " + TABLE_NAME + " where id_cedula=?";
            PreparedStatement preparedstatement = connection.prepareStatement(query);
            preparedstatement.setLong(1, id_cedula);
            preparedstatement.executeUpdate();
            preparedstatement.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }

    }

    /**
     *
     * @param id_cedula
     * @return
     */
    public Usuarios consultarUsuario(int id_cedula) {
        Usuarios usuarios = new Usuarios();

        try {
            conectar();

            String query = "select * from " + TABLE_NAME + " where id_cedula=?";
            PreparedStatement preparedstatement = connection.prepareStatement(query);
            preparedstatement.setLong(1, id_cedula);
            ResultSet resultset = preparedstatement.executeQuery();

            if (resultset.next()) {
                usuarios.setId_cedula(resultset.getInt("id_cedula"));
                usuarios.setNombre(resultset.getString("nombre"));
                usuarios.setApellido(resultset.getString("apellido"));
                usuarios.setContraseña(resultset.getString("contraseña"));
                usuarios.setProfesion(resultset.getString("profesion"));
                usuarios.setAdmin(resultset.getString("admin"));
                usuarios.setPrivilegio(resultset.getString("privilegio"));

            }

            resultset.close();
            preparedstatement.close();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }
        return usuarios;
    }
   
}
