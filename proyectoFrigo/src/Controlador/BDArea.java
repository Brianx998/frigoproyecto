/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.ConexionBD;
import Modelo.Area;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author sebas
 */
public class BDArea extends ConexionBD {

    public BDArea() {
        super();
    }

    private final String TABLE_NAME = "area";

    /**
     * 
     * @return 
     */
    public List<Area> consultarTodos() {
        List<Area> resultado = new ArrayList<>();

        try {
            conectar();
            String query = "select * from " + TABLE_NAME;
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Area area = new Area();
                area.setId_area(resultSet.getInt("id_area"));
                area.setDescripcion(resultSet.getString("descripcion"));

                resultado.add(area);

            }
            resultSet.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "error en la consulta sql " + e.getMessage());
        } finally {
            desconectar();
        }
        return resultado;
    }

    /**
     * 
     * @param area 
     */
    public void insertar(Area area) {
        try {
            conectar();
            String query = "insert into " + TABLE_NAME + " values (?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            int posicion = 0;

            preparedStatement.setInt(++posicion, area.getId_area());
            preparedStatement.setString(++posicion, area.getDescripcion());

            preparedStatement.executeUpdate();

            preparedStatement.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "error  " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "error en la consulta sql " + e.getMessage());
        } finally {
            desconectar();
        }
    }

    /**
     * 
     * @param area 
     */
    public void actualizarArea(Area area) {

        try {
            conectar();

            String query = "update " + TABLE_NAME + " set  descripcion=? where id_area=?";
            PreparedStatement preparedstatement = connection.prepareStatement(query);
            int pos = 0;

            preparedstatement.setString(++pos, area.getDescripcion());
            preparedstatement.setInt(++pos, area.getId_area());

            preparedstatement.executeUpdate();
            preparedstatement.close();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }

    }

    /**
     * 
     * @param id_area 
     */
    public void eliminarArea(int id_area) {

        try {
            conectar();

            String query = "delete from " + TABLE_NAME + " where id_area=?";
            PreparedStatement preparedstatement = connection.prepareStatement(query);
            preparedstatement.setLong(1, id_area);
            preparedstatement.executeUpdate();
            preparedstatement.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }

    }
/**
 * 
 * @param id_area
 * @return 
 */
    public Area consultarArea(int id_area) {
        Area area = new Area();

        try {
            conectar();

            String query = "select * from " + TABLE_NAME + " where id_area=?";
            PreparedStatement preparedstatement = connection.prepareStatement(query);
            preparedstatement.setLong(1, id_area);
            ResultSet resultset = preparedstatement.executeQuery();

            if (resultset.next()) {
                area.setId_area(resultset.getInt("id_area"));
                area.setDescripcion(resultset.getString("descripcion"));

            }

            resultset.close();
            preparedstatement.close();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }
        return area;
    }

    

}
