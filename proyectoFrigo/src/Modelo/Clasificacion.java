/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author braia
 */
public class Clasificacion {
    
    private int id_clasificacion;
    private String descripcion;

    public Clasificacion() {
    }

    public Clasificacion(int id_clasificacion, String descripcion) {
        this.id_clasificacion = id_clasificacion;
        this.descripcion = descripcion;
    }

    public int getId_clasificacion() {
        return id_clasificacion;
    }

    public void setId_clasificacion(int id_clasificacion) {
        this.id_clasificacion = id_clasificacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
}
