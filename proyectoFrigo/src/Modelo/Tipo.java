/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author braia
 */
public class Tipo {

    private int id_tipo;
    private String descripcion;
    private int id_area;
    private int id_clasificacion;

    public Tipo() {
    }

    public Tipo(int id_tipo, String descripcion, int id_area, int id_clasificacion) {
        this.id_tipo = id_tipo;
        this.descripcion = descripcion;
        this.id_area = id_area;
        this.id_clasificacion = id_clasificacion;
    }

    public int getId_tipo() {
        return id_tipo;
    }

    public void setId_tipo(int id_tipo) {
        this.id_tipo = id_tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getId_area() {
        return id_area;
    }

    public void setId_area(int id_area) {
        this.id_area = id_area;
    }

    public int getId_clasificacion() {
        return id_clasificacion;
    }

    public void setId_clasificacion(int id_clasificacion) {
        this.id_clasificacion = id_clasificacion;
    }
    
    

}
