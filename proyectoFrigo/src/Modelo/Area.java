/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author sebas
 */
public class Area {
    
    private int id_area;
    private String descripcion;

    public Area() {
    }

    public Area(int id_area, String descripcion) {
        this.id_area = id_area;
        this.descripcion = descripcion;
    }

   

    public int getId_area() {
        return id_area;
    }

    public void setId_area(int id_area) {
        this.id_area = id_area;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
    
    
}
