/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author braia
 */
public class Usuarios {

    private int id_cedula;
    private String nombre;
    private String apellido;
    private String contraseña;
    private String admin;
    private String privilegio;
    private String profesion;

    public Usuarios() {
    }

    public Usuarios(int id_cedula, String nombre, String apellido, String contraseña, String admin, String privilegio, String profesion) {
        this.id_cedula = id_cedula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.contraseña = contraseña;
        this.admin = admin;
        this.privilegio = privilegio;
        this.profesion = profesion;
    }

    public int getId_cedula() {
        return id_cedula;
    }

    public void setId_cedula(int id_cedula) {
        this.id_cedula = id_cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getPrivilegio() {
        return privilegio;
    }

    public void setPrivilegio(String privilegio) {
        this.privilegio = privilegio;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }
    
    
}
