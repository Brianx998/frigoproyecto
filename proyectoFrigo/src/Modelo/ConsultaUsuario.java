/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author braia
 */
public class ConsultaUsuario {
   
    private int id_consulta;
    private int id_cedula;
    private int id_tipo;
    private String fecha;

    public ConsultaUsuario() {
    }

    public ConsultaUsuario(int id_consulta, int id_cedula, int id_tipo) {
        this.id_consulta = id_consulta;
        this.id_cedula = id_cedula;
        this.id_tipo = id_tipo;
    }

    public int getId_consulta() {
        return id_consulta;
    }

    public void setId_consulta(int id_consulta) {
        this.id_consulta = id_consulta;
    }

    public int getId_cedula() {
        return id_cedula;
    }

    public void setId_cedula(int id_cedula) {
        this.id_cedula = id_cedula;
    }

    public int getId_tipo() {
        return id_tipo;
    }

    public void setId_tipo(int id_tipo) {
        this.id_tipo = id_tipo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
}
