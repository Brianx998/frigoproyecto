-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.11-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para bd_frigo
CREATE DATABASE IF NOT EXISTS `bd_frigo` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci */;
USE `bd_frigo`;

-- Volcando estructura para tabla bd_frigo.area
CREATE TABLE IF NOT EXISTS `area` (
  `id_area` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_frigo.area: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` (`id_area`, `descripcion`) VALUES
	(1, 'Bovinos'),
	(2, 'Porcinos'),
	(3, 'Contabilidad'),
	(4, 'SG-SST'),
	(5, 'Despachos');
/*!40000 ALTER TABLE `area` ENABLE KEYS */;

-- Volcando estructura para tabla bd_frigo.clasificacion
CREATE TABLE IF NOT EXISTS `clasificacion` (
  `id_clasificacion` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`id_clasificacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_frigo.clasificacion: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `clasificacion` DISABLE KEYS */;
INSERT INTO `clasificacion` (`id_clasificacion`, `descripcion`) VALUES
	(1, 'Manuales'),
	(2, 'Registros'),
	(3, 'Instrutivos'),
	(4, 'Formatos');
/*!40000 ALTER TABLE `clasificacion` ENABLE KEYS */;

-- Volcando estructura para tabla bd_frigo.consulta_usuario
CREATE TABLE IF NOT EXISTS `consulta_usuario` (
  `id_consulta` int(11) NOT NULL AUTO_INCREMENT,
  `id_cedula` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id_consulta`),
  KEY `FK-id_cedula` (`id_cedula`),
  KEY `FK-id_tipo` (`id_tipo`),
  CONSTRAINT `FK-id_cedula` FOREIGN KEY (`id_cedula`) REFERENCES `usuarios` (`id_cedula`),
  CONSTRAINT `FK-id_tipo` FOREIGN KEY (`id_tipo`) REFERENCES `tipo` (`id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_frigo.consulta_usuario: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `consulta_usuario` DISABLE KEYS */;
INSERT INTO `consulta_usuario` (`id_consulta`, `id_cedula`, `id_tipo`, `fecha`) VALUES
	(1, 1116280345, 47, '2021-12-20 15:26:28'),
	(2, 1116280392, 30, '2020-12-20 15:24:49');
/*!40000 ALTER TABLE `consulta_usuario` ENABLE KEYS */;

-- Volcando estructura para tabla bd_frigo.tipo
CREATE TABLE IF NOT EXISTS `tipo` (
  `id_tipo` int(11) NOT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `Id_area` int(11) DEFAULT NULL,
  `id_clasificacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_tipo`),
  KEY `Id_area` (`Id_area`),
  KEY `id_clasificacion` (`id_clasificacion`),
  CONSTRAINT `FK-id_area` FOREIGN KEY (`Id_area`) REFERENCES `area` (`id_area`),
  CONSTRAINT `FK-id_clasificacion` FOREIGN KEY (`id_clasificacion`) REFERENCES `clasificacion` (`id_clasificacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_frigo.tipo: ~60 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo` DISABLE KEYS */;
INSERT INTO `tipo` (`id_tipo`, `descripcion`, `Id_area`, `id_clasificacion`) VALUES
	(0, 'MN-B-01', 1, 1),
	(1, 'MN-B-02', 1, 1),
	(2, 'MN-B-03', 1, 1),
	(3, 'MN-P-01', 2, 1),
	(4, 'MN-P-02', 2, 1),
	(5, 'MN-P-03', 2, 1),
	(6, 'MN-Con-01', 3, 1),
	(7, 'MN-Con-02', 3, 1),
	(8, 'MN-Con-03', 3, 1),
	(9, 'MN-SGSST-01', 4, 1),
	(10, 'MN-SGSST-02', 4, 1),
	(11, 'MN-SGSST-03', 4, 1),
	(12, 'MN-Des-01', 5, 1),
	(13, 'MN-Des-02', 5, 1),
	(14, 'MN-Des-03', 5, 1),
	(15, 'RG-B-01', 1, 2),
	(16, 'RG-B-02', 1, 2),
	(17, 'RG-B-03', 1, 2),
	(18, 'RG-P-01', 2, 2),
	(19, 'RG-P-02', 2, 2),
	(20, 'RG-P-03', 2, 2),
	(21, 'RG-Con-01', 3, 2),
	(22, 'RG-Con-02', 3, 2),
	(23, 'RG-Con-03', 3, 2),
	(24, 'RG-SGSST-01', 4, 2),
	(25, 'RG-SGSST-02', 4, 2),
	(26, 'RG-SGSST-03', 4, 2),
	(27, 'RG-Des-01', 5, 2),
	(28, 'RG-Des-02', 5, 2),
	(29, 'RG-Des-03', 5, 2),
	(30, 'IN-B-01', 1, 3),
	(31, 'IN-B-02', 1, 3),
	(32, 'IN-B-03', 1, 3),
	(33, 'IN-P-01', 2, 3),
	(34, 'IN-P-02', 2, 3),
	(35, 'IN-P-03', 2, 3),
	(36, 'IN-Con-01', 3, 3),
	(37, 'IN-Con-02', 3, 3),
	(38, 'IN-Con-03', 3, 3),
	(39, 'IN-SGSST-01', 4, 3),
	(40, 'IN-SGSST-02', 4, 3),
	(41, 'IN-SGSST-03', 4, 3),
	(42, 'IN-Des-01', 5, 3),
	(43, 'IN-Des-02', 5, 3),
	(44, 'IN-Des-03', 5, 3),
	(45, 'FOR-B-01', 1, 4),
	(46, 'FOR-B-02', 1, 4),
	(47, 'FOR-B-03', 1, 4),
	(48, 'FOR-P-01', 2, 4),
	(49, 'FOR-P-02', 2, 4),
	(50, 'FOR-P-03', 2, 4),
	(52, 'FOR-Con-01', 3, 4),
	(53, 'FOR-Con-02', 3, 4),
	(54, 'FOR-Con-03', 3, 4),
	(55, 'FOR-SGSST-01', 4, 4),
	(56, 'FOR-SGSST-02', 4, 4),
	(57, 'FOR-SGSST-03', 4, 4),
	(58, 'FOR-Des-01', 5, 4),
	(59, 'FOR-Des-02', 5, 4),
	(60, 'FOR-Des-03', 5, 4);
/*!40000 ALTER TABLE `tipo` ENABLE KEYS */;

-- Volcando estructura para tabla bd_frigo.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_cedula` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `contraseña` varchar(50) DEFAULT NULL,
  `admin` varchar(2) NOT NULL DEFAULT '0',
  `privilegio` varchar(2) NOT NULL DEFAULT '',
  `profesion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_cedula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_frigo.usuarios: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id_cedula`, `nombre`, `apellido`, `contraseña`, `admin`, `privilegio`, `profesion`) VALUES
	(1116280345, 'sebastian', 'trung', '', '1', '1', NULL),
	(1116280392, 'Brian', 'Sanchez', '1234567', '0', '0', NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
